'use strict';

var Bird = function(game, x, y, frame) {
  Phaser.Sprite.call(this, game, x, y, 'bird', frame);
  // initialize your prefab here
  this.alive = true;
  this.game.physics.arcade.enableBody(this);
  this.anchor.setTo(0.5, 0.5);
  this.scale.set(0.5);
  this.animations.add('fly');
  this.animations.play('fly', 12, true);

  this.body.bounce.y = 0.5;  
};

Bird.prototype = Object.create(Phaser.Sprite.prototype);
Bird.prototype.constructor = Bird;

Bird.prototype.update = function() {
  
  // write your prefab's specific update code here
  if (this.alive && this.angle < 90) this.angle += 2.5;
  
};

Bird.prototype.flap = function () {
    this.bird.body.velocity.y = -400;
    this.game.add.tween(this.bird).to({angle: -40}, 100).start();
}

Bird.prototype.die = function (bird, pipes) {

    this.alive = false;
    this.loadTexture('flappy-crash', 0);
    this.animations.add('crash');
    this.animations.play('crash', 30, true);
}

module.exports = Bird;

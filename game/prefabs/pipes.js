'use strict';

var Pipes = function(game, x, y, frame) {
  Phaser.Sprite.call(this, game, x, y, 'pipe', frame);

  // initialize your prefab here
  this.anchor.setTo(0.5, 0.5);
  this.game.physics.arcade.enableBody(this);
  this.body.allowGravity = false;
  this.body.immovable = true;
  
};

Pipes.prototype = Object.create(Phaser.Sprite.prototype);
Pipes.prototype.constructor = Pipes;

Pipes.prototype.update = function() {
  
  // write your prefab's specific update code here
  
};

module.exports = Pipes;

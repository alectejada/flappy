'use strict';
var Pipe = require('./pipes');

var PipeGroup = function(game, parent) {
  Phaser.Group.call(this, game, parent);

  // initialize your prefab here
  this.topPipe = new Pipe(this.game, 0, 0, 0);
  this.add(this.topPipe);

  this.bottomPipe = new Pipe(this.game, 0, 440, 1);
  this.add(this.bottomPipe);

  this.setAll('body.velocity.x', -200);   
  this.hasScored = false;
};

PipeGroup.prototype = Object.create(Phaser.Group.prototype);
PipeGroup.prototype.constructor = PipeGroup;

PipeGroup.prototype.update = function() {
  
  // write your prefab's specific update code here
  
};

PipeGroup.prototype.reset = function (x) {

    var topPipeY = this.game.rnd.integerInRange(-100, 150);
    var hole = 125;

    this.topPipe.reset(x, topPipeY);

    this.bottomPipe.reset(x, topPipeY + hole + this.bottomPipe.height);

    this.setAll('body.velocity.x', -200);
    this.hasScored = false;
    this.exists = true;
}

PipeGroup.prototype.stop = function () {

    var velocity = this.topPipe.body.velocity.x;
    this.setAll('body.velocity.x', velocity / 3);
}

module.exports = PipeGroup;

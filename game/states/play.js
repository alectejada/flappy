
  'use strict';

  var Bird = require('../prefabs/bird'); 
  var Ground = require('../prefabs/ground'); 
  var PipeGroup = require('../prefabs/pipeGroup');

  function Play() {}

  Play.prototype = {

    preload: function () {

    },
    create: function() {

        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.physics.arcade.gravity.y = 1200;

        this.score = 0;  
        this.background = this.game.add.tileSprite(0, 0, 800,600, 'background');

        this.bird = new Bird(this.game, 100, this.game.height/2);
        this.game.add.existing(this.bird);

        this.pipes = this.game.add.group();
        this.timer = this.game.time.events.loop(1500, this.add_pipes, this);

        this.ground = new Ground(this.game, 0, this.game.height - 112, 800, 112);
        this.game.add.existing(this.ground);

        var style = { font: "40px Arial", fill: "#ffffff" };  
        this.label_score = this.game.add.text(20, 20, "0", style);  

        var space_key = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        space_key.onDown.add(this.bird.flap, this);  
    },
    update: function() {
        
        this.game.physics.arcade.collide(
            this.bird,
            this.ground,
            this.deathHandler.bind(this)
        );

        this.pipes.forEach(function(pipeGroup) {
            this.checkScore(pipeGroup);

            this.game.physics.arcade.collide(
                this.bird,
                pipeGroup,
                this.deathHandler.bind(this),
                null,
                this
            );
        }, this);
    },
    restart_game: function() {

        this.game.time.events.remove(this.timer); 

        setTimeout((function () {
            this.game.state.start('gameover');
        }).bind(this), 2000);
    },
    add_pipes: function () {

        var pipeGroup =  this.pipes.getFirstExists(false);

        if (!pipeGroup) {
            pipeGroup = new PipeGroup(this.game, this.pipes);
        }
        pipeGroup.reset(
            this.game.width + pipeGroup.width/2
        );
    },
    checkScore : function (pipe) {
        if(pipe.exists && !pipe.scored && this.bird.world.x >= pipe.bottomPipe.x) {
            pipe.scored = true;
            this.score += 1;  
            this.label_score.text = this.score; 
        }
    },
    deathHandler : function (bird, pipeGroup) {
        this.game.input.keyboard.removeKey(Phaser.Keyboard.SPACEBAR);
        this.bird.die();
        this.pipes.forEach( function (pipeGroup) {
            pipeGroup.stop();
        }); 
        this.ground.stop();
        this.restart_game();
    },
    shutdown: function () {
        this.game.input.keyboard.removeKey(Phaser.Keyboard.SPACEBAR);
        this.bird.destroy();
        this.pipes.destroy();
    }
  };
  
  module.exports = Play;


'use strict';
function Preload() {
  this.asset = null;
  this.ready = false;
}

Preload.prototype = {

  preload: function() {
    this.asset = this.add.sprite(this.width/2,this.height/2, 'preloader');
    this.asset.anchor.setTo(0.5, 0.5);

    this.load.onLoadComplete.addOnce(this.onLoadComplete, this);
    this.load.setPreloadSprite(this.asset);
    this.load.image('yeoman', 'assets/yeoman-logo.png');
    this.load.image('background', 'assets/background.png');
    this.load.image('ground', 'assets/ground.png');
    this.game.load.spritesheet('bird', 'assets/flappy-bird.png', 100, 97); 
    this.game.load.spritesheet('flappy-crash', 'assets/flappy-crash.png', 100, 97); 
    this.game.load.spritesheet('pipe', 'assets/pipes.png', 54, 320, 2);  

  },
  create: function() {
    this.asset.cropEnabled = false;
  },
  update: function() {
    if(!!this.ready) {
      this.game.state.start('menu');
    }
  },
  onLoadComplete: function() {
    this.ready = true;
  }
};

module.exports = Preload;
